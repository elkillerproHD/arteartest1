import React, { Component } from 'react';
import {InfiniteScroll} from "./src/containers/infiniteScroll";


export default class App extends Component {
  render() {
    return (
      <InfiniteScroll />
    );
  }
}
