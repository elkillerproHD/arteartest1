import React, { useState, useEffect } from 'react';
import axios from "axios";
import {Text, FlatList, Alert} from 'react-native'
import styled from 'styled-components/native'
import { vw, vh } from 'react-native-expo-viewport-units';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {Header} from "../components/header";
import {AltearItem} from "../components/item";

const Container = styled.View`
  width: ${vw(100)}px;
  height: ${vh(100) - getStatusBarHeight()}px;
  margin-top: ${getStatusBarHeight()}px};
`;
const EmptySpace = styled.View`
  width: ${vw(100)}px;
  height: ${vh(1)}px;
`;
function NoMoreContent ()  {
  Alert.alert(
    'Bueno, todo tiene su final',
    'No hay mas contenido que mostrar. Agradezco al cafe por brindarme el combustible para terminar esta app',
    [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );
}
export const InfiniteScroll = props => {
  let [questions, setQuestions] = useState([]);
  let [page, setPage] = useState(1);
  let [noMore, setNoMore] = useState(false);
  const MakeQuery = async () => {
    const res = await axios.get(`http://5e16456b22b5c600140cf9bf.mockapi.io/api/v1/questions?page=${page}&limit=10`);
    setQuestions(res.data)
    await setPage(page + 1);
  };
  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 100;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };
  const LoadMore = async () => {
    console.log(page);
    const res = await axios.get(`http://5e16456b22b5c600140cf9bf.mockapi.io/api/v1/questions?page=${page}&limit=10`);
    if(res.data.length > 0) {
      setQuestions([...questions, ...res.data])
      await setPage(page + 1);
    } else {
      if(!noMore){
        NoMoreContent();
        setNoMore(true);
        setTimeout(()=>{setNoMore(false);}, 15000)
      }
    }
  };
  useEffect(() => {
    MakeQuery().then();
  }, []);

  return (
    <Container>
      <Header/>
      <EmptySpace />
      <FlatList
        data={questions}
        extraData={questions}
        renderItem={({ item }) => <AltearItem number={item["numero"]} question={item["pregunta"]} answer={item["respuesta"]} />}
        keyExtractor={item => item.numero.toString()}
        onScroll={({nativeEvent}) => {
          if (isCloseToBottom(nativeEvent)) {
            LoadMore();
          }
        }}
        scrollEventThrottle={400}
      />
      <EmptySpace />
    </Container>
  )
};
