
import { Dimensions, Platform, PixelRatio } from 'react-native';

// Retrieve initial screen's width
export const screenWidth = Dimensions.get('window').width;

// Retrieve initial screen's height
export const screenHeight = Dimensions.get('window').height;

const desingHeight = 640;
const desingWidth = 360;

const scale = screenWidth / desingWidth;

const GetWidth = (width) => {
  const deltaPercentage = screenWidth / desingWidth;
  return deltaPercentage * width;
};

const GetHeight = (height) => {
  const deltaPercentage = screenHeight / desingHeight;
  return deltaPercentage * height;
};

/**
 * @return {number}
 */
export function Normalize(size) {
  const newSize = size * scale
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}

export {
  GetWidth,
  GetHeight
};
