import React from 'react';
import { TouchableOpacity, Alert } from "react-native";
import styled from 'styled-components/native'
import { vw, vh } from 'react-native-expo-viewport-units';
import ArtearLogo from '../images/logo-artear.png'
import OpenMenuIcon from '../svg/js/open-menu.svg.js'

const Container = styled.View `
  background-color: rgba(0,0,0,0.8);
  width: ${vw(100)}px;
  height: ${vh(14)}px;
  padding-horizontal: 5%;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
`;
const Image = styled.Image `
  width: ${vw(30)}px;
  height: ${vh(9)}px;
  resize-mode: contain;
`;

function Pressed ()  {
  Alert.alert(
    'Nice try my friend!',
    'This stuff do nothing. Absolutely nothing',
    [
      {text: 'OK', onPress: () => console.log('OK Pressed')},
    ],
    {cancelable: false},
  );
}

export const Header = props => {
  return (
    <Container>
      <Image source={ArtearLogo} />
      <TouchableOpacity onPress={Pressed}>
        <OpenMenuIcon fill={'#fff'} width={vw(9)} height={vw(9)} />
      </TouchableOpacity>
    </Container>
  )
} ;
