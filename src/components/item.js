import React from 'react';
import styled from 'styled-components/native'
import { vw, vh } from 'react-native-expo-viewport-units';
import {Normalize} from "../constants/size";

const Container = styled.View `
  width: ${vw(90)}px;
  margin-horizontal: 5%;
  background-color: #fff;
  padding: 3%;
  margin-top: ${vh(1)}px;
  margin-bottom: ${vh(1)}px;
`;
const Pregunta = styled.Text `
  font-weight: bold;
  font-size: ${Normalize(18)}px;
  color: #000;
`;
const Respuesta = styled.Text `
  font-size: ${Normalize(16)}px;
  color: #000;
`;
export const AltearItem = props => {

  return(
    <Container style={{
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      elevation: 4,
    }}>
      <Pregunta>{'#'+props.number+' - '+props.question}</Pregunta>
      <Respuesta>{'Respuesta: '+props.answer}</Respuesta>
    </Container>
  )
};
AltearItem.defaultProps = {
  number: 1,
  question: 'EN EL LENGUAJE DE CHAT ¿CUÁL DE ESTAS OPCIONES SUELE SIGNIFICAR “REÍRSE A CARCAJADAS”?',
  answer: 'LOL'
};
